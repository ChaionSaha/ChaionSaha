[![Typing SVG](https://readme-typing-svg.herokuapp.com?font=Silkscreen&size=100&pause=1000&color=80F7F6FF&center=true&width=2120&height=500&lines=Hi%2C+I+am+Chaion+Saha.;I+am+a+Front+End+Developer.;2%2B+years+of+coding+experience.;Welcome+to+my+gitlab+profile.)](https://git.io/typing-svg)


 

## 💫 About Me: 
 - <img src="https://user-images.githubusercontent.com/90419674/187185994-85e6aa78-54ae-4701-92e5-35e8afb3a34b.gif" height="20px"/> I am Chaion Saha.
 - 🏫 Currently Studying at Rajshahi University of Engineering and Technology.<br>
 - <img src="https://user-images.githubusercontent.com/90419674/187188685-985ee40f-6e86-4d20-be49-ab811f42e1e4.gif" width="20px" valign="center"/> Currently learning everything. <br>
 - <img src="https://user-images.githubusercontent.com/90419674/187188835-06811af1-c09f-4115-b4ff-ab54020f4abf.gif" width="20px" valign="center"/> Know a bit about **C#, HTML, CSS, JS**<br> 
 - 🥅 2022 goals: Be more productive.<br> 
 - ⚡ I love experiencing new things.<br> 
 - 📫 Email: **chaionsaha00@gmail.com**
 

 
 <br/>
<br/>


## 🤹🏻‍♂️ My Skill Set: 





<div align="left">  
<a href="https://en.wikipedia.org/wiki/HTML5" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/html5-original-wordmark.svg" alt="HTML5" height="50" /></a>  
<a href="https://www.w3schools.com/css/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/css3-original-wordmark.svg" alt="CSS3" height="50" /></a>  
<a href="https://www.javascript.com/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/javascript-original.svg" alt="JavaScript" height="50" /></a>  
<a href="https://www.cprogramming.com/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/c-original.svg" alt="C" height="50" /></a>  
<a href="https://www.cplusplus.com/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/cplusplus-original.svg" alt="C++" height="50" /></a>  
<a href="https://docs.microsoft.com/en-us/dotnet/csharp/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/csharp-original.svg" alt="C#" height="50" /></a>  
<a href="https://getbootstrap.com/docs/3.4/javascript/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/bootstrap-plain.svg" alt="Bootstrap" height="50" /></a>  
<a href="https://www.tailwindcss.com/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/tailwindcss.svg" alt="Tailwind CSS" height="50" /></a>  
<a href="https://sass-lang.com/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/sass-original.svg" alt="Sass" height="50" /></a>  
</div>


 

<br/>  
<br/>

## ☎️ Connect with me:  
<div align="left">
<a href="https://github.com/ChaionSaha" target="_blank">
<img src=https://img.shields.io/badge/github-%2324292e.svg?&style=for-the-badge&logo=github&logoColor=white alt=github style="margin-bottom: 5px;" />
</a>
<a href="https://www.facebook.com/chaion.saha.mp" target="_blank">
<img src=https://img.shields.io/badge/facebook-%232E87FB.svg?&style=for-the-badge&logo=facebook&logoColor=white alt=facebook style="margin-bottom: 5px;" />
</a>
<a href="https://linkedin.com/in/chaion-saha-63b0b21a5" target="_blank">
<img src=https://img.shields.io/badge/linkedin-%231E77B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white alt=linkedin style="margin-bottom: 5px;" />
</a>  
</div>  
  

<br/>  
<br/>

<!--
## 📊 Github Stats:  

<div align="center"><img src="https://github-readme-stats.vercel.app/api?username=ChaionSaha&show_icons=true&count_private=true&hide_border=true" align="center" /></div>  

 !-->
<!--
![ChaionSaha's gitlab activity graph](https://activity-graph.herokuapp.com/graph?username=ChaionSaha&theme=gitlab)
!-->
<br/>  

  

<br/>  

  

<br/> 
<hr>


<br/>  


